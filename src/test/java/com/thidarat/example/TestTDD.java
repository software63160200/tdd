/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.thidarat.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class TestTDD {
    
    public TestTDD() {
    }
     @Test
     public void testChop_p1P_p_p2P_is_draw(){
         assertEquals("draw", Example.chup('p','p'));
     }
     @Test
     public void testChop_p1P_h_p2P_is_draw(){
         assertEquals("draw", Example.chup('h','h'));
     }
     @Test
     public void testChop_p1P_s_p2P_p_is_p1(){
         assertEquals("p1", Example.chup('s','p'));
     }
     @Test
     public void testChop_p1P_h_p2P_s_is_p1(){
         assertEquals("p1", Example.chup('h','s'));
     }
     @Test
     public void testChop_p1P_p_p2P_h_is_p1(){
         assertEquals("p1", Example.chup('p','h'));
     }
     @Test
     public void testChop_p1P_s_p2P_h_is_p2(){
         assertEquals("p2", Example.chup('s','h'));
     }
     @Test
     public void testChop_p1P_p_p2P_s_is_p2(){
         assertEquals("p2", Example.chup('p','s'));
     }

}
